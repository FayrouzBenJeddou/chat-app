import logo from './logo.svg';
import './App.css';
import {useState } from "react"
import { ChatEngine ,getOrCreateChat  } from 'react-chat-engine';

function App() {
  const [username, setUsername] = useState('')

	function createDirectChat(creds) {
		getOrCreateChat(
			creds,
			{ is_direct_chat: true, usernames: [username] },
			() => setUsername('')
		)
	}

	function renderChatForm(creds) {
		return (
			<div style={{display : "flex", justifyContent:"space-between"  , margin :"0.5rem"  }}>
				<input 
					placeholder='Username' 
					value={username} 
					onChange={(e) => setUsername(e.target.value)} 
          style={{width : "100%", padding :"0.5rem 1rem" , borderRadius :"25px 0 0 25px ", border :"none" , border : "1px solid #1890ff"}}
				/>
				<button onClick={() => createDirectChat(creds)}   style={{  cursor : "pointer" , padding :"0.5rem 1rem" , background :"#1890ff" , color: "white",border : "none" , borderRadius :"0 25px 25px 0 "}}>
					Create
				</button>
			</div>
		)
	}
  return (
    <div className="App">
    <ChatEngine
 			projectID='57b6c9b4-fe79-467c-a17e-c11e227286e8'
      ///changes goes here
 			userName='Farah'
 			userSecret='123456789'
      height='100vh'
      renderNewChatForm={(creds) => renderChatForm(creds)}


 		/>
    </div>
  );
}

export default App;
